const express = require('express');
const nanoid = require('nanoid');
const router = express.Router();
const fileMs = require('../fileMs');

router.get('/messages', (req, res)=> {
    if (req.query.date) {
      res.send(fileMs.getMesageDate(req.query.date))
    } else {
        res.send(fileMs.getMessage())
    }

});


router.post('/messages', (req, res) => {
    if (req.body.author !== '' && req.body.message !== '') {
        const date = new Date().toISOString();

        req.body.id = nanoid();
       req.body.datetime = date;
       fileMs.addMessage(req.body);
       res.send({message: 'ok'})
    } else {
      res.status(400).send({message: 'Author and message must be present in the request'})
    }
});


module.exports = router;