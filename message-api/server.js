const express = require('express');
const cors = require('cors');
const app = express();
const messages = require('./app/messages');
const fileMs = require('./fileMs');

const port = 8000;

app.use(express.json());
app.use(cors());
fileMs.init();


app.use('/', messages);



app.listen(port, () => {
    console.log(`Server starded on ${port} port`);
});
