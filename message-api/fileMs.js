const fs = require('fs');

const fileMessages = './messages.json';

let data = [];

module.exports ={
    init() {
        try {
            const fileContent = fs.readFileSync(fileMessages);
            data = JSON.parse(fileContent)
        } catch (e) {
            data = []
        }
    },
    getMessage() {
        return data.slice(data.length - 30)
    },
    addMessage(item) {
        data.push(item);
        this.save()
    },
    save() {
        fs.writeFileSync(fileMessages, JSON.stringify(data, null, 2))
    },
    getMesageDate(date) {
        const result = data.filter(message => message.datetime > date);
        return result
    }
};