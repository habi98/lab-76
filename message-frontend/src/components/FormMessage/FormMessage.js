import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Row} from "reactstrap";

class FormMessage extends Component {
    state = {
      author: '',
      message: ''
    };

    valueChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    formHandler = (event) => {
        event.preventDefault()
        this.props.onSubmit({...this.state})
    };


    render() {
        return (
        <Form onSubmit={this.formHandler}>
            <Row form className="pt-1">
                <Col md={5}>
                    <FormGroup>
                        <Input value={this.state.author} onChange={this.valueChange} type="text" name="author"  placeholder="Author" />
                    </FormGroup>
                </Col>
                <Col md={5}>
                    <FormGroup>
                        <Input value={this.state.message} onChange={this.valueChange} type="text" name="message"  placeholder="Message" />
                    </FormGroup>
                </Col>
                <Col md={2}>
                    <FormGroup>
                        <Button>Add</Button>
                    </FormGroup>
                </Col>
            </Row>
        </Form>
        );
    }
}

export default FormMessage;