import React, { Component } from 'react';
import {NotificationContainer} from 'react-notifications'
import './App.css';
import {Container} from "reactstrap";
import NewMessage from "./containers/NewMessage/NewMessage";
import Messages from "./containers/Messages/Messages";

class App extends Component {
  render() {
    return (
        <Container>
            <NotificationContainer/>
           <NewMessage/>
            <Messages/>
        </Container>

    );
  }
}

export default App;
