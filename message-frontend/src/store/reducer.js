import {CREACTE_MESSAGE_ERROR, FETCH_MESSAGES_SUCCESS, GET_MESSAGE_SUCCESS} from "./action";

const initialState = {
    messages: [],
    error: null
};

const reducer = (state = initialState, action) => {
   switch (action.type) {
       case FETCH_MESSAGES_SUCCESS:
           return {...state, messages: action.messages};
       case GET_MESSAGE_SUCCESS:
           return {...state, messages: [...state.messages].concat(action.message)};
       case CREACTE_MESSAGE_ERROR:
           return {...state, error: action.error};
       default:
           return state
   }
};

export default reducer