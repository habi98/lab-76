import axios from '../axios-message-api';

export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS ';
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});

export const CREATE_MESSAGE_SUCCESS = 'CREATE_MESSAGE_SUCCESS';
export const createToMessage = () => ({type: CREATE_MESSAGE_SUCCESS});

export const CREACTE_MESSAGE_ERROR = 'CREACTE_MESSAGE_ERROR';

export const createToMessageError = error => ({type: CREACTE_MESSAGE_ERROR, error});

export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';

export const getToMessage = message => ({type: GET_MESSAGE_SUCCESS, message});



export const fetchMessages = () => {
  return dispatch => {
      axios.get('/messages').then(response => {
         dispatch(fetchMessagesSuccess(response.data))
      })
  }
};


export const createMessage = dataMessage => {
    return dispatch => {
        return axios.post('/messages', dataMessage).then(response => {
            dispatch(createToMessage())
        }, error => dispatch(createToMessageError(error.response.data.message)))
    };
}

export const getMessage = date => {
    return dispatch => {
            axios.get(`/messages?date=${date}`).then(response => {
                dispatch(getToMessage(response.data))
            })
    }
};