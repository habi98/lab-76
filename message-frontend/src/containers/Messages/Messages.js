import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchMessages, getMessage} from "../../store/action";
import {Card, CardBody, CardText, CardTitle} from "reactstrap";


class Messages extends Component {
    componentDidMount() {
        this.props.onFetchMessages()
    }

    getToMessage = () => {
        const date = this.props.messages[this.props.messages.length - 1].datetime;
        this.interval = setInterval(() => {
            this.props.getMessage(date)
        }, 2000)
    } ;
    componentDidUpdate() {
        clearInterval(this.interval);
        this.getToMessage()
    }

    render() {
        return (
            <Fragment>
                {this.props.messages.map((message, id) => (
                    <Card key={id} className="mt-3">
                        <CardBody>
                            <CardTitle>{message.datetime}</CardTitle>
                            <CardText>[Author: {message.author}] {message.message}</CardText>
                        </CardBody>
                    </Card>
                ))}
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    messages: state.messages
});

const mapDispatchToProps = dispatch => ({
    onFetchMessages: () => dispatch(fetchMessages()),
    getMessage: (date) => dispatch(getMessage(date))

});

export default  connect(mapStateToProps, mapDispatchToProps)(Messages);