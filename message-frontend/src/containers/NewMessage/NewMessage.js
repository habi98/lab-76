import React, {Component, Fragment} from 'react';
import FormMessage from "../../components/FormMessage/FormMessage";
import {connect} from "react-redux";
import {NotificationManager} from 'react-notifications'
import {createMessage} from "../../store/action";

class NewMessage extends Component {
    createdMessage = dataMessage => {
        this.props.createMessage(dataMessage).then(() => {
            this.props.error && NotificationManager.error(this.props.error, 'error')
        })
    };
    render() {
        return (
            <Fragment>
                <h3 className="pt-4 pb-3">Messages</h3>
                <FormMessage onSubmit={this.createdMessage}/>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
   error: state.error
});

const mapDispatchToProps = dispatch => ({
    createMessage: dataMessage => dispatch(createMessage(dataMessage))
});


export default connect(mapStateToProps, mapDispatchToProps)(NewMessage);